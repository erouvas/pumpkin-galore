#!/usr/bin/python

#LED1 - 18 - 24
#LED2 - 16 - 23
#LED3 - 15 - 22
#LED4 - 40 - 21
#LED5 - 38 - 20
#LED6 - 35 - 19
#LED7 - 12 - 18
#LED8 - 11 - 17
#LED9 - 36 - 16
#LED10 - 33 - 13
#LED11 (GREEN) - 31 - 6
#LED12 (GREEN) - 32 - 12

from pumpkinpi import PumpkinPi
from time import sleep
from signal import pause

pumpkin = PumpkinPi(pwm=True)
leds = pumpkin.leds

#pumpkin = PumpkinPi(pwm=True)
#pumpkin.sides.left.bottom.pulse(5, 0.5, 1) # Fade in for 5 seconds, fade out for .5 seconds and do this once.
#pumpkin.sides.right.bottom.pulse(5, 0.5, 1)
#sleep(2)
pumpkin.off()

step = [2,3,4,5,0,6,11,1,10,9,8,7]
step_reverse = [7,8,9,10,1,11,6,0,5,4,3,2]
step_no_eyes = [2,3,4,5,6,11,10,9,8,7]

loop = [1,2,3,4]
pumpkin.off()

while True:
  for i in [1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1]:
      pumpkin.off()
      if i % 2 == 0:
        pumpkin.eyes.left.pulse(5, 0.5, 1)
        pumpkin.sides.right.pulse(i, 0.1, 1)
      else:
        pumpkin.eyes.right.pulse(5, 0.5, 1)
        pumpkin.sides.left.pulse(i, 0.1, 1)
      sleep(0.7)
  pumpkin.off()    
 
  pumpkin.sides.pulse(5, 0.5, 1)
  pumpkin.eyes.on()
  for i in loop:
      pumpkin.sides.on()
      sleep(0.2)
      pumpkin.sides.pulse(5, 0.5, 1)
      sleep(0.2)
  pumpkin.eyes.pulse(5, 0.5, 1)
  sleep(2)
  pumpkin.off()
  
  
  for index in loop:
    pumpkin.sides.left.bottom.on() 
    pumpkin.sides.right.top.on() 
    sleep(0.1)
    pumpkin.sides.left.bottom.pulse(5, 0.5, 1)
    pumpkin.sides.right.top.pulse(5, 0.5, 1)
    #
    pumpkin.sides.left.midbottom.on() 
    pumpkin.sides.right.midtop.on() 
    sleep(0.1)
    pumpkin.sides.left.midbottom.pulse(5, 0.5, 1)
    pumpkin.sides.right.midtop.pulse(5, 0.5, 1)
    #
    pumpkin.sides.left.middle.on() 
    pumpkin.sides.right.middle.on() 
    sleep(0.1)
    pumpkin.sides.left.middle.pulse(5, 0.5, 1)
    pumpkin.sides.right.middle.pulse(5, 0.5, 1)
    #
    pumpkin.sides.left.midtop.on()
    pumpkin.sides.right.midbottom.on() 
    sleep(0.1)
    pumpkin.sides.left.midtop.pulse(5, 0.5, 1)
    pumpkin.sides.right.midbottom.pulse(5, 0.5, 1)
    #
    pumpkin.sides.left.top.on()
    pumpkin.sides.right.bottom.on() 
    sleep(0.1)
    pumpkin.sides.left.top.pulse(5, 0.5, 1)
    pumpkin.sides.right.bottom.pulse(5, 0.5, 1)
  pumpkin.eyes.blink(.1, .1, 0, 0, 12) # Blink on for .1 seconds, off for .1 seconds, do not fade and do this 12 times.
  sleep(1.5)
  pumpkin.off() 
  
  try:
      pumpkin.eyes.blink(.1, .1, 0, 0, 12) # Blink on for .1 seconds, off for .1 seconds, do not fade and do this 12 times.
      pumpkin.sides.left.bottom.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.right.bottom.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.left.midbottom.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.right.midbottom.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.left.middle.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.right.middle.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.left.midtop.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.right.midtop.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.left.top.blink(.1, .1, 0, 0, 12)
      pumpkin.sides.right.top.blink(.1, .1, 0, 0, 12)
      sleep(3)
  except KeyboardInterrupt:
          pumpkin.off() 
  
  pumpkin.off()
  
  try:
          for index in step_no_eyes:
                  leds[index].on()
                  sleep(0.2)
          pumpkin.eyes.blink(.1, .1, 0, 0, 12) # Blink on for .1 seconds, off for .1 seconds, do not fade and do this 12 times.
          sleep(3)
  except KeyboardInterrupt:
          pumpkin.off()
  
  try:
          for index in step_reverse:
                  leds[index].off()
                  sleep(0.2)
          pumpkin.eyes.blink(.1, .1, 0, 0, 12) # Blink on for .1 seconds, off for .1 seconds, do not fade and do this 12 times.
          sleep(3)
  except KeyboardInterrupt:
          pumpkin.off()
  
  
  try:
          for index in step:
                  leds[index].on()
                  sleep(0.2)
                  leds[index].off()
  except KeyboardInterrupt:
          pumpkin.off()
          pumpkin.close()
else:
    pumpkin.off()
    
pumkin.clsoe()